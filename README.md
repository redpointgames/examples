# THIS PROJECT HAS MOVED

Due to [upcoming GitLab changes](https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/), this repository has permanently moved to: [https://src.redpoint.games/redpointgames/examples](https://src.redpoint.games/redpointgames/examples).

The repository on GitLab will no longer be monitored, maintained or receive updates. You should update your Git submodule URLs and bookmarks.
